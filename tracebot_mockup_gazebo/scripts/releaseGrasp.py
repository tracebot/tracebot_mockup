#!/usr/bin/env python
import rospy
import actionlib

# Interface with tracebot
from tracebot_msgs.msg import ReleaseGraspAction, ReleaseGraspResult, ReleaseType
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal, FollowJointTrajectoryResult





class ReleaseGrasp(object):
    # create message used to publish result
    _result = ReleaseGraspResult()

    def __init__(self, name):
        self.hand = hand
        self._action_name = "release_grasp_%s" % (self.hand)
        self._as = actionlib.SimpleActionServer(self._action_name, ReleaseGraspAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()

        # initialize gripper action client
        self.client = actionlib.SimpleActionClient(
            '%s_gripper_joint_trajectory_controller/follow_joint_trajectory' % (self.hand), FollowJointTrajectoryAction)
        self.client.wait_for_server()

    def execute_cb(self, goal):

        try:
            # publish info to the console for the user
            rospy.loginfo('%s: Executing release grasp, with goal %s' % (self._action_name, goal.release_grasp_type.release_type_id))

            # parse the action goal :
            release_type_id = goal.release_grasp_type.release_type_id
            if release_type_id == ReleaseType.POSITION:
                release_position = 0
            elif release_type_id == ReleaseType.CUSTOM:
                release_position = goal.release_grasp_type.custom_grasp_release[0]
            else:
                release_position = 0
                rospy.logwarn(
                    "%s : Release type id unknown or not implemented : %s . Default release position (%s) will be chosen." % (
                    self._action_name, release_type_id, release_position))
            max_close_position = 0.9
            if release_position > max_close_position or release_position < 0:
                rospy.logerr(
                    "%s : Release position out of range : %s . For Robotiq gripper, it should be between 0 (open) and %s (close)" % (
                    self._action_name, release_position, max_close_position))
                release_grasp_status = False
                self._result.release_grasp_status = release_grasp_status
                self._as.set_aborted(self._result)

            # execute the grasping
            release_grasp_status = self.set_gripper(release_position)


            # Pack the result
            self._result.release_grasp_status = release_grasp_status
            rospy.loginfo('%s: Succeeded, result: %s' % (self._action_name, self._result))
            self._as.set_succeeded(self._result)

        except rospy.ROSException as e:
            rospy.loginfo("Action call failed: %s" % (e))

    def set_gripper(self, position):
        trajectory = JointTrajectory()
        trajectory.joint_names.append("tracebot_%s_gripper_right_driver_joint" % (self.hand))
        point = JointTrajectoryPoint()
        point.time_from_start = rospy.Duration(1)
        point.positions.append(position)
        point.velocities.append(0)
        point.accelerations.append(0)
        trajectory.points.append(point)
        goal = FollowJointTrajectoryGoal()
        goal.trajectory = trajectory

        self.client.send_goal(goal)
        self.client.wait_for_result()
        res = self.client.get_result()
        return res.error_code == FollowJointTrajectoryResult.SUCCESSFUL

if __name__ == '__main__':
    # The name of the gripper can be passed as an argument ("right" or "left")
    try:
        args = rospy.myargv()
        if len(args) < 2:
            hand = ""
        else:
            hand = args[1]
        rospy.init_node("release_grasp_%s"%(hand),log_level=rospy.INFO)
        rospy.loginfo("Starting server %s ... ", rospy.get_name())
        server = ReleaseGrasp(hand)
        rospy.spin()
    except rospy.ROSInterruptException:
        rospy.loginfo("Program interrupted before completion")