#!/usr/bin/env python

import actionlib
import rospy

from tracebot_msgs.msg import LocateObjectAction, LocateObjectGoal


def vision_mockup_client():
    client = actionlib.SimpleActionClient('/vision_locate_object', LocateObjectAction)
    client.wait_for_server()

    # Creates a goal to send to the action server.
    goal = LocateObjectGoal(object_to_locate='Pump')
    client.send_goal(goal)

    client.wait_for_result()
    return client.get_result()

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('vision_mockup_test_client')
        result = vision_mockup_client()
        rospy.loginfo(result.header)
        rospy.loginfo(result.object_poses)
        rospy.loginfo(result.object_types)
        rospy.loginfo(result.confidences)
        rospy.loginfo("{result.color_image.width} X {result.color_image.height}".format(**locals()))
        rospy.loginfo("{result.depth_image.width} X {result.depth_image.height}".format(**locals()))
        rospy.loginfo(result.camera_info)
    except rospy.ROSInterruptException:
        rospy.logerr("program interrupted before completion")
