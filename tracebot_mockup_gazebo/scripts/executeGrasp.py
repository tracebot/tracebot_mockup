#!/usr/bin/env python
import rospy
import actionlib

# Interface with tracebot
from tracebot_msgs.msg import ExecuteGraspAction, ExecuteGraspResult, CloseType
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal, FollowJointTrajectoryResult



class ExecuteGrasp(object):
    # create message used to publish result
    _result = ExecuteGraspResult()

    def __init__(self, hand):
        self.hand = hand
        self._action_name = "execute_grasp_%s"%(self.hand)
        self._as = actionlib.SimpleActionServer(self._action_name, ExecuteGraspAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()

        # initialize gripper action client
        self.client = actionlib.SimpleActionClient('%s_gripper_joint_trajectory_controller/follow_joint_trajectory'%(self.hand), FollowJointTrajectoryAction)
        self.client.wait_for_server()
    def execute_cb(self, goal):

        try:
            # publish info to the console for the user
            rospy.loginfo('%s: Executing execute grasp, with goal %s' % (self._action_name, goal.close_grasp_type.close_type_id))

            # parse the action goal :
            close_type_id = goal.close_grasp_type.close_type_id
            if close_type_id == CloseType.POSITION:
                close_position = 0.5
            elif close_type_id == CloseType.CUSTOM:
                close_position = goal.close_grasp_type.custom_grasp_close[0]
            else:
                close_position = 0.8
                rospy.logwarn("%s : Close type id unknown or not implemented : %s . Default closing position (%s) will be chosen." %(self._action_name, close_type_id, close_position ))
            max_close_position = 0.9
            if close_position>max_close_position or close_position<0:
                rospy.logerr("%s : Closing position out of range : %s . For Robotiq gripper, it should be between 0 (open) and %s (close)" %(self._action_name, close_position, max_close_position))
                close_grasp_status = False
                self._result.close_grasp_status = close_grasp_status
                self._as.set_aborted(self._result)



            # execute the grasping
            close_grasp_status = self.set_gripper(close_position)

            # Pack the result
            self._result.close_grasp_status = close_grasp_status
            rospy.loginfo('%s: Completed, result: %s' % (self._action_name, self._result))
            self._as.set_succeeded(self._result)


        except rospy.ROSException as e:
            rospy.loginfo("Action call failed: %s" % (e))


    def set_gripper(self, position):
        trajectory = JointTrajectory()
        trajectory.joint_names.append("tracebot_%s_gripper_right_driver_joint" % (self.hand))
        point = JointTrajectoryPoint()
        point.time_from_start = rospy.Duration(1)
        point.positions.append(position)
        point.velocities.append(0)
        point.accelerations.append(0)
        trajectory.points.append(point)
        goal = FollowJointTrajectoryGoal()
        goal.trajectory = trajectory

        self.client.send_goal(goal)
        self.client.wait_for_result()
        res = self.client.get_result()
        return res.error_code == FollowJointTrajectoryResult.SUCCESSFUL


if __name__ == '__main__':
    # The name of the gripper can be passed as an argument ("right" or "left")
    try:
        args = rospy.myargv()
        if len(args) < 2:
            hand = ""
        else:
            hand = args[1]
        rospy.init_node("execute_grasp_%s"%(hand),log_level=rospy.INFO)
        rospy.loginfo("Starting server %s ... ", rospy.get_name())
        server = ExecuteGrasp(hand)
        rospy.spin()
    except rospy.ROSInterruptException:
        rospy.loginfo("Program interrupted before completion")