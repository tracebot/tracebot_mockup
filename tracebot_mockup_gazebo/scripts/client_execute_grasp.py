#!/usr/bin/env python
import rospy
import actionlib

from tracebot_msgs.msg import ExecuteGraspAction, ExecuteGraspGoal, CloseType


def dummy_client(hand):
    # Creates the SimpleActionClient, passing the type of the action to the constructor.
    client = actionlib.SimpleActionClient("execute_grasp_%s"%(hand), ExecuteGraspAction)

    # Creates a goal to send to the action server.
    close_type_id = CloseType.POSITION
    custom_grasp_close = []
    # close_type_id = CloseType.CUSTOM
    # custom_grasp_close = [0.7]
    close_grasp_type = CloseType(close_type_id = close_type_id, custom_grasp_close = custom_grasp_close)
    goal = ExecuteGraspGoal(close_grasp_type = close_grasp_type)

    # Waits until the action server has started up and started listening for goals.
    client.wait_for_server()

    # Sends the goal to the action server.
    client.send_goal(goal)

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result()  # A dummy_action result

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        # The name of the gripper can be passed as an argument ("right" or "left")
        args = rospy.myargv()
        if len(args) < 2:
            hand = ""
        else:
            hand = args[1]
        rospy.init_node("dummy_execute_client_%s"%(hand))
        rospy.loginfo("Launching client %s ...", rospy.get_name())
        result = dummy_client(hand)
        rospy.loginfo("%s : Result: %s", rospy.get_name(), result)
    except rospy.ROSInterruptException:
        rospy.loginfo("program interrupted before completion")