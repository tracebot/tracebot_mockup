#!/usr/bin/env python

import actionlib
import copy
import message_filters
import rospy
import tf2_geometry_msgs  # needed, registers geometry_msgs as valid inputs
                          #  for tf2_ros.Buffer().transform
import tf2_ros

from gazebo_msgs.srv import GetModelState
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import Image, CameraInfo
from tracebot_msgs.msg import LocateObjectAction, LocateObjectFeedback, LocateObjectResult


class LocateObjectMockupAction(object):
    def __init__(self, name):
        # Define objects the node is able to localize
        self.localizable_objects = ['Canister', 'Pump']

        # Set up camera topics
        self.color_cam_topic = "/tracebot_camera/color/image_raw"
        self.color_cam_info_topic = "/tracebot_camera/color/camera_info"
        self.depth_cam_topic = "/tracebot_camera/depth/image_raw"
        self.depth_cam_info_topic = "/tracebot_camera/depth/camera_info"

        # Obtain the camera intrinsics,
        # assuming they do not change during the action execution
        self.camera_info = rospy.wait_for_message(self.color_cam_info_topic,
                                                  CameraInfo)

        # Creates a tf2 listener to obtain transforms
        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

        # Use TimeSynchroniser to generate synced pairs of color and depth images
        color_sub = message_filters.Subscriber(self.color_cam_topic, Image)
        depth_sub = message_filters.Subscriber(self.depth_cam_topic, Image)

        ts = message_filters.ApproximateTimeSynchronizer([color_sub, depth_sub], 1, 1)
        ts.registerCallback(self.ts_callback)
        self.color_img, self.depth_img = None, None

        self.gz_get_model_state_topic = "/gazebo/get_model_state"

        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, LocateObjectAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()

        rospy.loginfo('%s: Action Server Ready' % self._action_name)

    def ts_callback(self, color_image, depth_image):
        # Gets us a synchronised pair of color and depth image
        self.color_img = color_image
        self.depth_img = depth_image

    def query_gazebo(self, model_name, relative_entity_name='tracebot_base_link'):
        rospy.wait_for_service(self.gz_get_model_state_topic)

        try:
            gz_get_state = rospy.ServiceProxy(self.gz_get_model_state_topic, GetModelState)
            resp1 = gz_get_state(model_name, relative_entity_name)
            return resp1
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)


    def transform_state(self, model_state, dest_frame="tracebot_camera_color_optical_frame"):
        pose_to_transform = PoseStamped()
        pose_to_transform.header = model_state.header
        pose_to_transform.pose = model_state.pose

        transformed = self.tfBuffer.transform(pose_to_transform, dest_frame, timeout=rospy.Duration(10.0))
        return transformed

    def execute_cb(self, goal):
        # helper variables
        result = LocateObjectResult()
        r = rospy.Rate(10)
        success = False

        # Making sure images are available
        for i in range(10):
            if self.color_img:
                success = True
                break
            r.sleep()

        if not success:
            rospy.loginfo('%s: Unable to obtain images, action call aborted' % (self._action_name))
            self._as.set_aborted()

        if goal.object_to_locate not in self.localizable_objects:
            rospy.loginfo('%s: Unknown goal.object_to_locate, action call aborted. Only the following objects can be detected: %s' % (self._action_name, ','.join(self.localizable_objects)))
            self._as.set_aborted()
            success = False

        # start executing the action
        # check that preempt has not been requested by the client
        if self._as.is_preempt_requested():
            rospy.loginfo('%s: Preempted' % self._action_name)
            self._as.set_preempted()
            success = False

        if success:
            color_img, depth_img = copy.deepcopy(self.color_img), copy.deepcopy(self.depth_img)

            if goal.object_to_locate != "":
                rospy.loginfo('%s: Executing, locating object %s' % (self._action_name, goal.object_to_locate))

                result.object_types = [goal.object_to_locate]
                model_states = [self.query_gazebo(goal.object_to_locate)]
            else:
                rospy.loginfo('%s: Executing, locating all objects' % (self._action_name))
                model_states = []
                print("self.localizable_objects", self.localizable_objects)
                for model_name in self.localizable_objects:
                    state = self.query_gazebo(model_name)
                    if state.success:
                        result.object_types.append(model_name)
                        model_states.append(state)

            result.header = color_img.header  # std_msgs/Header header
            for model_state in model_states:
                transformed_pose = self.transform_state(model_state)
                result.object_poses.append(transformed_pose.pose)  # geometry_msgs/Pose[] object_poses
                result.confidences.append(1.)  # float32[] confidences
            result.color_image = color_img  # sensor_msgs/Image color_image
            result.depth_image = depth_img  # sensor_msgs/Image depth_image
            result.camera_info = self.camera_info  # sensor_msgs/CameraInfo camera_info


            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(result)


if __name__ == '__main__':
    rospy.init_node('vision_locate_object')
    server = LocateObjectMockupAction(rospy.get_name())
    rospy.spin()
