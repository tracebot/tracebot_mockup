#!/usr/bin/env python
import rospy
import actionlib

# Interface with tracebot
from tracebot_msgs.msg import PreshapeGraspAction, PreshapeGraspResult



class PreshapeGrasp(object):
    # create message used to publish result
    _result = PreshapeGraspResult()

    def __init__(self, hand):
        self.hand = hand
        self._action_name = "preshape_grasp_%s"%(self.hand)
        self._as = actionlib.SimpleActionServer(self._action_name, PreshapeGraspAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()

    def execute_cb(self, goal):

        try:
            # publish info to the console for the user
            rospy.loginfo('%s: Executing preshape grasp, with goal %s' % (self._action_name, goal.grasp_type.grasp_type_id))

            # Compute the result
            grasp_id_status = True

            # Pack the result
            self._result.grasp_id_status = grasp_id_status

            rospy.loginfo('%s: Succeeded, result: %s' % (self._action_name, self._result))

            self._as.set_succeeded(self._result)

        except rospy.ROSException as e:
            rospy.loginfo("Action call failed: %s" % (e))

if __name__ == '__main__':
    # The name of the gripper can be passed as an argument ("right" or "left")
    try:
        args = rospy.myargv()
        if len(args) < 2:
            hand = ""
        else:
            hand = args[1]
        rospy.init_node("preshape_grasp_%s"%(hand),log_level=rospy.INFO)
        rospy.loginfo("Starting server %s ... ", rospy.get_name())
        server = PreshapeGrasp(hand)
        rospy.spin()
    except rospy.ROSInterruptException:
        rospy.loginfo("Program interrupted before completion")