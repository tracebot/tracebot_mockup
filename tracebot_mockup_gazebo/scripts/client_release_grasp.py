#!/usr/bin/env python
import rospy
import actionlib

from tracebot_msgs.msg import ReleaseGraspAction, ReleaseGraspGoal, ReleaseType


def dummy_client(hand):
    # Creates the SimpleActionClient, passing the type of the action to the constructor.
    client = actionlib.SimpleActionClient("release_grasp_%s"%(hand), ReleaseGraspAction)

    # Creates a goal to send to the action server.
    release_type_id = ReleaseType.POSITION
    custom_grasp_release = []
    # release_type_id = ReleaseType.CUSTOM
    # custom_grasp_release = [0.2]
    release_grasp_type = ReleaseType(release_type_id = release_type_id, custom_grasp_release = custom_grasp_release)
    goal = ReleaseGraspGoal(release_grasp_type = release_grasp_type)

    # Waits until the action server has started up and started listening for goals.
    client.wait_for_server()

    # Sends the goal to the action server.
    client.send_goal(goal)

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result()  # A dummy_action result

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        # The name of the gripper can be passed as an argument ("right" or "left")
        args = rospy.myargv()
        if len(args) < 2:
            hand = ""
        else:
            hand = args[1]
        rospy.init_node("dummy_release_client_%s"%(hand))
        rospy.loginfo("Launching client %s ...", rospy.get_name())
        result = dummy_client(hand)
        rospy.loginfo("%s : Result: %s", rospy.get_name(), result)
    except rospy.ROSInterruptException:
        rospy.loginfo("program interrupted before completion")