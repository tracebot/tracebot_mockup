<?xml version="1.0"?>
<launch>
  <!-- Launch file for loading the tracebot mockup into gazebo. Making use of the UR launch file
  as a template for the nescesarry things to include. -->
  <arg name="camera_tilt" default="-0.524"/>
  <arg name="camera_mount_offset_z" default="0.221"/>

  <arg name="pump_pos_x" default="-0.3"/>
  <arg name="pump_pos_y" default="0.0"/>

  <arg name="robotiq_grippers" default="true"/>
  <arg name="left_arm" default="true"/>
  <arg name="right_arm" default="true"/>

  <arg name="left_arm_joint_limits_parameters_file" default="$(find ur_description)/config/ur10e/joint_limits.yaml"/>
  <arg name="left_arm_kinematics_parameters_file" default="$(find ur_description)/config/ur10e/default_kinematics.yaml"/>
  <arg name="left_arm_physical_parameters_file" default="$(find ur_description)/config/ur10e/physical_parameters.yaml"/>
  <arg name="left_arm_visual_parameters_file" default="$(find ur_description)/config/ur10e/visual_parameters.yaml"/>
  <arg name="right_arm_joint_limits_parameters_file" default="$(find ur_description)/config/ur10e/joint_limits.yaml"/>
  <arg name="right_arm_kinematics_parameters_file" default="$(find ur_description)/config/ur10e/default_kinematics.yaml"/>
  <arg name="right_arm_physical_parameters_file" default="$(find ur_description)/config/ur10e/physical_parameters.yaml"/>
  <arg name="right_arm_visual_parameters_file" default="$(find ur_description)/config/ur10e/visual_parameters.yaml"/>

  <arg name="rviz" default="false"/>

  <arg name="pub_clock_frequency" default="100"/>

  <!--Load robot description-->
  <include file="$(find tracebot_mockup_gazebo)/launch/load_tracebot_mockup_description.launch" pass_all_args="true"/>

  <!-- robot_state_publisher configuration -->
  <arg name="tf_prefix" default="" doc="tf_prefix used for the robot."/>
  <arg name="tf_pub_rate" default="500" doc="Rate at which robot_state_publisher should publish transforms."/>

  <!-- Gazebo parameters -->
  <arg name="paused" default="true" doc="Starts Gazebo in paused mode" />
  <arg name="gui" default="true" doc="Starts Gazebo gui" />

  <!-- Load robot controllers -->
  <include file="$(find tracebot_controllers)/launch/load_tracebot_base_controllers.launch" pass_all_args="true"/>
  <include file="$(find tracebot_controllers)/launch/load_tracebot_sim_gripper_controllers.launch" pass_all_args="true"/>

  <!-- Robot state publisher -->
  <node pkg="robot_state_publisher" type="robot_state_publisher" name="robot_state_publisher">
    <param name="publish_frequency" type="double" value="$(arg tf_pub_rate)" />
    <param name="tf_prefix" value="$(arg tf_prefix)" />
  </node>

  <!-- Start the 'driver' (ie: Gazebo in this case) (Making use of existing ur file for launch)-->
  <!--include file="$(find ur_gazebo)/launch/inc/ur_control.launch.xml">
    <arg name="controller_config_file" value="$(arg controller_config_file)"/>
    <arg name="controllers" value="$(arg controllers)"/>
    <arg name="gui" value="$(arg gui)"/>
    <arg name="paused" value="$(arg paused)"/>
    <arg name="stopped_controllers" value="$(arg stopped_controllers)"/>
    <arg name="gazebo_world" value="$(find tracebot_mockup_gazebo)/worlds/tracebot_mockup_world.world"/>
  </include-->

  <!-- Gazebo parameters -->
  <arg name="gazebo_model_name" default="robot" doc="The name to give to the model in Gazebo (after spawning it)." />
  <arg name="gazebo_world" default="$(find tracebot_mockup_gazebo)/worlds/tracebot_mockup_world.world" doc="The '.world' file to load in Gazebo." />

  <arg name="robot_description_param_name" default="robot_description" doc="Name of the parameter which contains the robot description (ie: URDF) which should be spawned into Gazebo." />
  <arg name="spawn_z" default="0.1" doc="At which height the model should be spawned. NOTE: lower values will cause the robot to collide with the ground plane." />
  <arg name="start_gazebo" default="true" doc="If true, Gazebo will be started. If false, Gazebo will be assumed to have been started elsewhere." />

  <!-- Start Gazebo and load the empty world if requested to do so -->
  <include file="$(find gazebo_ros)/launch/empty_world.launch" if="$(arg start_gazebo)">
    <arg name="verbose" value="true"/>
    <arg name="world_name" value="$(arg gazebo_world)"/>
    <arg name="paused" value="$(arg paused)"/>
    <arg name="gui" value="$(arg gui)"/>
    <arg name="use_clock_frequency" value="true"/>
    <arg name="pub_clock_frequency" value="$(arg pub_clock_frequency)"/>
  </include>

  <!-- Spawn the model loaded earlier in the simulation just started -->
  <node name="spawn_gazebo_model" pkg="gazebo_ros" type="spawn_model"
    args="
      -urdf
      -param $(arg robot_description_param_name)
      -model $(arg gazebo_model_name)
      -z $(arg spawn_z)"
    output="screen" respawn="false" />

  <node name="rviz" pkg="rviz" type="rviz" args="-d $(find tracebot_mockup_gazebo)/config/tracebot_mockup.rviz" if="$(arg rviz)"/>

  <!-- Mockup LocateObject provider -->
  <node name="vision_locate_object" type="mockup_vision_action_server.py" pkg="tracebot_mockup_gazebo"/>

  <!-- Mockup Gripper-related actions provider -->
      <!-- PreshapeGrasp action -->
  <node pkg="tracebot_mockup_gazebo" type="preshapeGrasp.py" name="server_preshape_grasp_right" args="right"/>
  <node pkg="tracebot_mockup_gazebo" type="preshapeGrasp.py" name="server_preshape_grasp_left" args="left"/>

      <!-- ExecuteGrasp action -->
  <node pkg="tracebot_mockup_gazebo" type="executeGrasp.py" name="server_execute_grasp_right" args="right"/>
  <node pkg="tracebot_mockup_gazebo" type="executeGrasp.py" name="server_execute_grasp_left" args="left"/>

      <!-- ReleaseGrasp action -->
  <node pkg="tracebot_mockup_gazebo" type="releaseGrasp.py" name="server_release_grasp_right" args="right"/>
  <node pkg="tracebot_mockup_gazebo" type="releaseGrasp.py" name="server_release_grasp_left" args="left"/>

</launch>
