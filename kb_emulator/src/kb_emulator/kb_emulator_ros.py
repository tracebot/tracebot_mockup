#!/usr/bin/env python
"""
@package kb_emulator
@file kb_emulator_ros.py
@author Anthony Remazeilles
@brief Emulation of the KR common sense

Copyright (C) 2022 Tecnalia Research and Innovation
beerware
"""

from copy import deepcopy
import rospy
import actionlib

# ROS message & services includes
from tracebot_msgs.msg import KBCommonPoseAction

# other includes
from kb_emulator import kb_emulator_impl


class KbEmulatorROS(object):
    """
    ROS interface class, handling all communication with ROS
    """
    def __init__(self):
        """
        Attributes definition
        """
        self.component_data_ = kb_emulator_impl.KbEmulatorData()
        self.component_config_ = kb_emulator_impl.KbEmulatorConfig()
        self.component_implementation_ = kb_emulator_impl.KbEmulatorImplementation()

        # handling parameters from the parameter server
        self.component_config_.kb_flag = rospy.get_param("~kb_flag", "kb_emul")
        # to enable action name adjustment when loading the node
        remap = rospy.get_param("~get_kb_common_pose_remap", "get_kb_common_pose")
        self.component_implementation_.passthrough.as_get_kb_common_pose = actionlib.SimpleActionServer(remap,
                                                                                                KBCommonPoseAction,
                                                                                                execute_cb=self.component_implementation_.callback_get_kb_common_pose,
                                                                                                auto_start=False)
        self.component_implementation_.passthrough.as_get_kb_common_pose.start()

    def configure(self):
        """
        function setting the initial configuration of the node
        """
        return self.component_implementation_.configure(self.component_config_)

    def activate_all_output(self):
        """
        activate all defined output
        """
        pass

    def set_all_input_read(self):
        """
        set related flag to state that input has been read
        """
        pass

    def update(self, event):
        """
        @brief update function

        @param      self The object
        @param      event The event

        @return { description_of_the_return_value }
        """
        self.activate_all_output()
        config = deepcopy(self.component_config_)
        data = deepcopy(self.component_data_)
        self.set_all_input_read()
        self.component_implementation_.update(data, config)


def main():
    """
    @brief Entry point of the package.
    Instanciate the node interface containing the Developer implementation
    @return nothing
    """
    rospy.init_node("kb_emulator", anonymous=False)

    node = KbEmulatorROS()
    if not node.configure():
        rospy.logfatal("Could not configure the node")
        rospy.logfatal("Please check configuration parameters")
        rospy.logfatal("{}".format(node.component_config_))
        return

    rospy.Timer(rospy.Duration(1.0 / 100.0), node.update)
    rospy.spin()
