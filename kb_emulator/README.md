# kb_emulator

## General description of the package

<!--- protected region package description begin -->
Emulation of the KR common sense
<!--- protected region package description end -->

<!--- todo How to handle the image generation -->
<!--- <img src="./model/kb_emulator.png" width="300px" />-->

## Node: kb_emulator

Update frequency: 100.0 Hz.

<!--- protected region kb_emulator begin -->
<!--- protected region kb_emulator end -->

### Static Parameters

All static parameters can be set through the command line:

```shell
rosrun kb_emulator kb_emulator [param_name]:=[new_value]
```

`kb_flag` *(std::string, default: "kb_emul")*
<!--- protected region param kb_flag begin -->
tag for the KB data in parameter server
<!--- protected region param kb_flag end -->

### Action proposed

A simple action launched can be obtained with:

```shell
rosrun actionlib axclient.py /do_action
```

Any action name can be readjusted at node launch:

```shell
rosrun kb_emulator kb_emulator _[old_name]:=[new_name]
```

`get_kb_common_pose` *(tracebot_msgs::KBCommonPose)*
<!--- protected region action server get_kb_common_pose begin -->
Emulate request of object grasping info
<!--- protected region action server get_kb_common_pose end -->

---

*Package generated with the [ROS Package Generator](https://github.com/tecnalia-advancedmanufacturing-robotics/ros_pkg_gen).*
