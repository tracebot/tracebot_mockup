# Tracebot Mockup

This repository contains ROS packages to run a simulated mockup of the Tracebot setup in Rviz and Gazebo.
The objective is to analyse different placements of the robots and equipment in regard to reachability and manipulability.

![Tracebot mockup screenshot](.res/tracebot_gazebo_v3.png)

## Table of Contents

- [Model Parameters](#model-parameters)
- [Usage](#usage)
  - [Launch Gazebo](#launch-gazebo)
- [Setup](#setup)
  - [Installing locally](#installing-locally)
  - [Using docker images](#using-docker-images)

## Model Parameters

The URDF model contained in `tracebot_mockup_description` is parametrized using xacro.
The table below lists the available parameters and their meaning.

| Parameter name | Description |
| -------------- | ----------- |
| `robot_name` | Set the prefix name of the robot, 'tracebot' by default. |
| `mount_base_height` | Height of the robot stand. |
| `camera_mount_offset_z` | Offset in Z direction of the camera with respect to the top of the stand. |
| `camera_tilt` | The forward tilt of the camera. |
| `rviz` | `Launch rviz with gazebo (false by defalut).` |
| `controller_config_file` | `Config file used for defining the ROS-Control controllers.` |
| `controllers` | `Controllers that are activated by default.` |
| `stopped_controllers` | `Controllers that are initally loaded, but not started.` |
| `tf_prefix` | `tf_prefix used for the robot.` |
| `tf_pub_rate` | `Rate at which robot_state_publisher should publish transforms.` |
| `paused` | `Starts Gazebo in paused mode. Paused by defaul to prevent objects spawning before table and falling to ground` |
| `gui` | `Starts Gazebo gui.` |
| `pump_pos_x` | X Position of pump on table (table center 0,0) |
| `pump_pos_y` | Y Position of pump on table (table center 0,0) |

All parameters use SI units.

## Usage

### Launch Gazebo

- This can be launched with the default using:

  ```bash
  roslaunch tracebot_mockup_gazebo view_tracebot_gazebo.launch
  ```

- Or using any of the available parameters, such as starting the simulation paused:

  ```bash
  roslaunch tracebot_mockup_gazebo view_tracebot_gazebo.launch paused:=true
  ```

The arms can then be communicated with by publishing to the `/pos_joint_traj_controller/command` topic which uses [JointTrajectory messages](http://docs.ros.org/en/noetic/api/trajectory_msgs/html/msg/JointTrajectory.html).
An example of this can be found in the [scripts/gazebo_model_test.py](tracebot_mockup_gazebo/scripts/gazebo_model_test.py) file.

To run this, and test that the gazebo simulation is working, run:

```bash
rosrun tracebot_mockup_gazebo gazebo_model_test.py
```

You should see that all the motors trigger and the arms curl up.

Rviz can also be set to launch with the gazebo simulation using:

```bash
roslaunch tracebot_mockup_gazebo view_tracebot_gazebo.launch rviz:=true
```

If you wish to launch rviz seperately (e.g. if launching from a seperate device) there is also a launch file in the gazebo package for this:

```bash
roslaunch tracebot_mockup_gazebo view_tracebot_rviz.launch
```

## Setup

There's two different possibilities to use the packages in this repository: installing locally or running the pre-built docker images.

### Installing locally

These packages are known to support ROS Melodic and Noetic, although other distros may be supported as well.

The packages can be built/installed following standard ROS-based procedures.
The examples listed below use [catkin_tools](https://catkin-tools.readthedocs.io) as build tool, although `catkin_make` and `catkin_make_isolated` _should_ be also supported:

- Create a workspace:

  ```bash
  mkdir -p ~/path/to/tracebot_mockup_ws/src
  cd ~/path/to/tracebot_mockup_ws
  catkin config --extend /opt/ros/"$ROS_DISTRO"
  ```

- Pull the packages into the repository:

  ```bash
  cd ~/path/to/tracebot_mockup_ws/src
  git clone https://gitlab.com/tracebot/tracebot_mockup.git
  ```

- Certain dependencies are not released as binary packages to either Melodic or Noetic, pull those into the workspace as well:

  ```bash
  cd ~/path/to/tracebot_mockup_ws/src
  vcs import < tracebot_mockup/upstream.repos # Install python(3)-vcstool if not available
  ```

- Install the rest of dependencies from binary repositories

  ```bash
  cd ~/path/to/tracebot_mockup_ws
  rosdep install -iy --from-paths src --rosdistro "$ROS_DISTRO"
  ```

- Then finally build everything:

  ```bash
  cd ~/path/to/tracebot_mockup_ws
  catkin build
  ```

### Using docker images

The `miguelprada/tracebot_mockup:noetic` docker image with the mockup packages pre-installed is periodically built and made available through DockerHub.

The recommended way to run this image is using the [osrf/rocker](https://github.com/osrf/rocker) tool.
This is a small wrapper tool around the docker CLI which facilitates tasks such as enabling X11 forwarding (required to view the simulated scene in rviz).

The mockup with default parameters can be run using rocker with:

```bash
rocker --x11 -- miguelprada/tracebot_mockup:noetic roslaunch tracebot_mockup_description view_tracebot_mockup.launch
```

Note that if your system runs NVidia graphics, you may need to use:

```bash
rocker --x11 --nvidia -- miguelprada/tracebot_mockup:noetic roslaunch tracebot_mockup_description view_tracebot_mockup.launch
```

Alternatively, instructions to enable X11 forwarding in docker in different ways can be found in [this page](http://wiki.ros.org/docker/Tutorials/GUI).
